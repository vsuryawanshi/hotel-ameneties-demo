import React, { Component } from 'react';
import PropTypes from "prop-types";
import {isMobile} from "../common/utils";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

const muiTheme = getMuiTheme({
    palette: {
        primary1Color: "#2196f3",
        pickerHeaderColor:"#2196f3"
    }
});

export default class App extends Component {
    render(){
        return (
            <MuiThemeProvider muiTheme={muiTheme}>
                <div className="page-wrap">
                    {
                        isMobile() ?

                        <div className="mobile-canvas">
                            <img src={require("../images/support.png")}/>
                            <div className="msg">
                                This demo currently does not support mobile, please view our desktop version
                            </div>
                        </div>
                        :
                        this.props.children
                    }
                </div>    
            </MuiThemeProvider>
        );
    }
}
App.propTypes = {
    children : PropTypes.node
};