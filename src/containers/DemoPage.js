import RaisedButton from 'material-ui/RaisedButton';
import React, { Component } from "react";
import Autocomplete from "../components/Common/Autocomplete";
import AutocompleteCity from "../components/Common/AutocompleteCity";
import NumberIncrement from "../components/Common/NumberIncrement";
import DatePicker from 'material-ui/DatePicker';
import moment from 'moment';
import axios from "axios";
import LinearProgress from 'material-ui/LinearProgress';
import "leaflet/dist/leaflet.css";
import { divIcon,latLngBounds} from 'leaflet';
import { Map, Marker, Popup, TileLayer } from 'react-leaflet';

export default class Demopage extends Component {
    constructor(props){
        super(props);
        this.state = {
            apiCallInProgress:false,
            hotelName:"",
            fromIp:"",
            fromId:"",
            fromLocation:"",
            startDate:null,
            endDate:null,
            hotelId:"",
            hotelLocation:"",
            adultCount:0,
            childCount:0,
            hasData:false,
            fromCityResponse:null,
            toCityResponse:null,
            hotelResponse:null,
            inferenceParamResponse:null,
            hotelAmenityResponse:[],
            localPlacesResponse:null,
            placesOfInterest:[]
        };
    }

    componentDidMount(){
        this.getUserIpAddress();
    }

    getUserIpAddress(){
        axios.get("http://ipinfo.io").then((response)=>{
            console.log(response);
            this.setState({
                fromIp:response.data.ip,
                fromLocation:response.data.loc
            });
        }).catch((err)=>{
            console.log(err);
        });
    }

    getUserLocation(){
        if(navigator.geolocation){
            navigator.geolocation.getCurrentPosition((position)=>{
                console.log(position);
            },(error)=>{
                switch(error.code) {
                    case error.PERMISSION_DENIED:
                        x.innerHTML = "User denied the request for Geolocation."
                        break;
                    case error.POSITION_UNAVAILABLE:
                        x.innerHTML = "Location information is unavailable."
                        break;
                    case error.TIMEOUT:
                        x.innerHTML = "The request to get user location timed out."
                        break;
                    case error.UNKNOWN_ERROR:
                        x.innerHTML = "An unknown error occurred."
                        break;
                }
            });
        } else {
            console.log("geolocation not supported");
        }
    }
    onAutocompleteTextChanged(newText){
        //empty function   
    }

    onAutocompleteItemSelected(selectedItem){
        console.log(selectedItem); 
        var loc = selectedItem.location.lat.toString() + "," + selectedItem.location.lon;
        this.setState({
            hotelLocation:loc,
            hotelName:selectedItem.name,
            hotelId:selectedItem.hotelId
        });
    }

    formatDate(date){
        return moment(date).format("DD-MM-YYYY");
    }

    getColor(){ 
        return '#'+ Math.floor(Math.random()*16777215).toString(16);
    }

    makeApiCall(){
        this.setState({
            apiCallInProgress:true
        },()=>{
            axios({
                method:"GET",
                url:`http://62.210.93.54:8010/api/getTravelDetails?frId=${this.state.fromId}&s_Dt=${this.formatDate(this.state.startDate)}&e_Dt=${this.formatDate(this.state.endDate)}&hn=${encodeURIComponent(this.state.hotelName)}&a_count=${this.state.adultCount}&c_count=${this.state.childCount}&hLoc=${encodeURIComponent(this.state.hotelLocation)}&compact=false`
            }).then((response)=>{
                if(response.data.localPlaces && response.data.localPlaces.attraction){
                    response.data.localPlaces.attraction.map((att)=>{
                        response.data.localPlaces.pois.push(att);
                    });
                }
                //get unique pois
                if(response.data.localPlaces && response.data.localPlaces.pois){
                    var temparr = [];
                    response.data.localPlaces.pois.map((poi)=>{
                        var tempObj = {};
                        var found = temparr.filter(item => item.type == poi.doc_type);
                        if(found.length == 0){
                            tempObj.type = poi.doc_type;
                            tempObj.color = this.getColor();
                            temparr.push(tempObj);
                        }
                    });
                    temparr.map((arr)=>{
                        response.data.localPlaces.pois.map((poi)=>{
                            if(poi.doc_type == arr.type){
                                poi.color = arr.color;
                            }
                        })
                    });
                }
                

                // //segregating bus stops and subways
                // if(response.data.localPlaces && response.data.localPlaces.transport.public_transport.length > 0){
                //     response.data.localPlaces.transport.public_transport.length.map((pt)=>{

                //     })   
                // }

                if(response.data.hotel.languagesSpoken.length > 0){
                    //filter out the languages spoken
                    var srcLanguage = response.data.travelDetailsResponse.fromCityDetails.language;
                    var destLanguage = response.data.travelDetailsResponse.toCityDetails.language;
                    var lspoken = []
                    if(response.data.hotel.languagesSpoken.indexOf(srcLanguage) !== -1){
                        lspoken.push(srcLanguage);
                    }
                    if(response.data.hotel.languagesSpoken.indexOf("English") !== -1){
                        if(srcLanguage !== "English"){
                            lspoken.push("English");
                        }
                    }
                    response.data.hotel.filterLanguageSpoken = lspoken;
                } else {
                    response.data.hotel.filterLanguageSpoken = [];
                }

                //gather the preferred cuisines
                var destinationCuisines = [];
                response.data.localPlaces.pois.map((poi)=>{
                    if(poi.doc_type == "triprestaurant" && poi.categories.length > 0){
                        poi.categories.map((cat)=>{
                            if(destinationCuisines.indexOf(cat) == -1){
                                destinationCuisines.push(cat);
                            }
                        });
                    }
                });
                var srcCuisines = response.data.travelDetailsResponse.fromCityDetails.cuisines;
                var finalCuisines = [];
                srcCuisines.map((sc)=>{
                    if(destinationCuisines.indexOf(sc) !== -1){
                        finalCuisines.push(sc);
                    }
                });
                response.data.inferenceParam.finalCuisines = finalCuisines;
                this.setState({
                    apiCallInProgress:false,
                    hasData:true,
                    fromCityResponse:response.data.travelDetailsResponse.fromCityDetails,
                    toCityResponse:response.data.travelDetailsResponse.toCityDetails,
                    hotelResponse:response.data.hotel,
                    inferenceParamResponse:response.data.inferenceParam,
                    hotelAmenityResponse:(response.data.hotelAmenity !== null ? response.data.hotelAmenity : []),
                    localPlacesResponse:response.data.localPlaces,
                    placesOfInterest:temparr
                },()=>{
                    setTimeout(()=>{
                        this.fitBoundsOnMap();
                    },400);
                });
            }).catch((err)=>{
                console.log(err.response);
                this.setState({
                    apiCallInProgress:false,
                    hasData:false,
                    fromCityResponse:null,
                    toCityResponse:null,
                    hotelResponse:null,
                    inferenceParamResponse:null,
                    hotelAmenityResponse:null,
                    localPlacesResponse:null
                });    
            });
        });
    }

    fitBoundsOnMap(){
        if (this.refs.map && this.refs.map.leafletElement && this.state.localPlacesResponse.pois) {
            var _bounds = latLngBounds(this.refs.hotel.leafletElement.getLatLng());
            for(var i=0 ;i < this.state.localPlacesResponse.pois.length; i++){
                _bounds.extend(this.refs["marker_" + i].leafletElement.getLatLng());
            }
            this.refs.map.leafletElement.fitBounds(_bounds);
        }
    }

    render(){
        var markerHtml = "<div class='hotel-marker'></div>";
        var icon = divIcon({
            className: 'location-marker',
            html:markerHtml});
        return(
            <div className="demo-wrap">
            {
                this.state.apiCallInProgress ?
                    <LinearProgress mode="indeterminate" style={{position:"fixed",top:0,left:0,right:0}}/> 
                    :
                    null
            }
                <div className="results-wrap">
                    <h1 className="heading">Inference Engine Demo</h1>
                    <div className={`settings-wrap`}>
                        <div className="toggle-btn" style={{"display":"none"}} onClick={(event)=>{
                            event.preventDefault();
                            event.stopPropagation();
                            this.setState({showSettingBar:!this.state.showSettingBar});
                        }}>
                            <img src={require("../images/nut-icon.svg")} className={`toggle-img` + (!this.state.showSettingBar ? " rot" : "")}/>    
                        </div> 
                        <div className="setting-header">
                            <img src={require("../images/nut-icon.svg")} className="cog" /> Settings
                        </div> 
                        <div className="setting-cont">
                            <div className="form-comp">
                            <AutocompleteCity placeholder="Enter Source Location" onQueryTextChanged={()=>{}} onItemSelected={(selectedItem)=>{
                                console.log(selectedItem);
                                this.setState({
                                    fromId:selectedItem.geonameId
                                });
                            }}/>
                            </div>
                            <div className="form-comp">
                                <Autocomplete placeholder="Enter Hotel name" onQueryTextChanged={this.onAutocompleteTextChanged.bind(this)} onItemSelected={this.onAutocompleteItemSelected.bind(this)}/>
                            </div>
                            <div className="form-comp w2">
                                <DatePicker 
                                    hintText="Start Date" 
                                    value={this.state.startDate}
                                    container="inline" 
                                    textFieldStyle={{width:"100%"}}
                                    onChange={(event,date)=>{
                                        this.setState({
                                            startDate:date
                                        });
                                    }}
                                    formatDate={(cdate)=>{
                                        return moment(cdate).format("ddd DD MMM, YYYY")
                                    }}/>
                            </div>
                            <div className="form-comp w2">
                                <DatePicker 
                                    value={this.state.endDate}
                                    hintText="End Date" 
                                    container="inline" 
                                    textFieldStyle={{width:"100%"}}
                                    onChange={(event,date)=>{
                                        this.setState({
                                            endDate:date
                                        });
                                    }}
                                    formatDate={(cdate)=>{
                                        return moment(cdate).format("ddd DD MMM, YYYY")
                                    }}/>
                            </div>
                            <div className="form-comp compact">
                                <NumberIncrement label="Adults :" icon={require("../images/boss.svg")} startValue={0} onValueChanged={(newVal)=>{
                                    this.setState({adultCount:newVal});
                                }}/>
                            </div>
                            <div className="form-comp compact">
                                <NumberIncrement label="Children :" icon={require("../images/boy.svg")} startValue={0} onValueChanged={(newVal)=>{
                                    this.setState({childCount:newVal});
                                }}/>
                            </div>
                            <div className="form-action">
                                <RaisedButton label="Submit" primary={true} onClick={()=>{
                                    this.makeApiCall();
                                }}/>
                            </div>
                        </div>
                    </div>

                    {
                        this.state.hasData ?

                        <div className="cards-wrap">
                            {
                                this.state.fromCityResponse !== null ?

                                <div className="card">
                                    <div className="card-header">Source Details</div>
                                    <div className="card-image" style={{backgroundImage:"url( " + require("../images/location.svg") + ")"}}/>
                                    <div className="card-body">
                                        <div className="rowitem">
                                            <div className="lbl">Source:</div>
                                            <div className="val">{this.state.fromCityResponse.cityName + ", " + this.state.fromCityResponse.country}</div>
                                        </div>
                                        {
                                            this.state.fromCityResponse.cabs ?
                                            <div className="rowitem">
                                                <div className="lbl">Cabs:</div>
                                                <div className="val">{this.state.fromCityResponse.cabs.join(", ")}</div>
                                            </div>
                                            :
                                            <div className="rowitem">
                                                <div className="lbl">Cabs:</div>
                                                <div className="val">No Cabs present</div>
                                            </div>
                                        }
                                        <div className="rowitem">
                                            <div className="lbl">Language:</div>
                                            <div className="val">{this.state.fromCityResponse.language}</div>
                                        </div>
                                        <div className="rowitem">
                                            <div className="lbl">Mobile Networks:</div>
                                            <div className="val">{this.state.fromCityResponse.networkOperator.join(", ")}</div>
                                        </div>
                                        <div className="rowitem">
                                            <div className="lbl">Beach Present:</div>
                                            <div className="val">{this.state.fromCityResponse.hasBeach ? " Yes" : " No"}</div>
                                        </div>
                                        <div className="rowitem">
                                            <div className="lbl">Mountain Present:</div>
                                            <div className="val">{this.state.fromCityResponse.hasMountain ? " Yes" : " No"}</div>
                                        </div>
                                        <div className="rowitem">
                                            <div className="lbl">Threat Level:</div>
                                            <div className="val">{this.state.fromCityResponse.cityThreat + "%"}</div>
                                        </div>
                                        <div className="rowitem">
                                            <div className="lbl">Available Cuisines:</div>
                                            <div className="val">{this.state.fromCityResponse.cuisines.join(", ")}</div>
                                        </div>
                                        <div className="rowitem">
                                            <div className="lbl">Temperature (min/max):</div>
                                            <div className="val">{this.state.fromCityResponse.weatherCityParam.wt_past_1_min + " / " + this.state.fromCityResponse.weatherCityParam.wt_past_1_max + " °C"}</div>
                                        </div>
                                        {
                                            this.state.fromCityResponse.cityLongWeekend ? 
                                            <div className="rowitem">
                                                <div className="lbl">Long Weekend:</div>
                                                <div className="val">Yes</div>
                                            </div>
                                            :
                                            null
                                        }
                                    </div>
                                </div>
                                :
                                null
                            }

                            {
                                this.state.toCityResponse !== null ?

                                <div className="card">
                                    <div className="card-header">Destination Details</div>
                                    <div className="card-image" style={{backgroundImage:"url( " + require("../images/pin.svg") + ")"}}/>
                                    <div className="card-body">
                                        <div className="rowitem">
                                            <div className="lbl">Destination:</div>
                                            <div className="val">{this.state.toCityResponse.cityName + "," + this.state.toCityResponse.country}</div>
                                        </div>
                                        {
                                            this.state.toCityResponse.cabs ? 
                                            <div className="rowitem">
                                                <div className="lbl">Cabs:</div>
                                                <div className="val">{this.state.toCityResponse.cabs.join(", ")}</div>
                                            </div>
                                            :
                                            <div className="rowitem">
                                                <div className="lbl">Cabs:</div>
                                                <div className="val">No cabs present</div>
                                            </div>
                                        }
                                        <div className="rowitem">
                                            <div className="lbl">Language:</div>
                                            <div className="val">{this.state.toCityResponse.language}</div>
                                        </div>
                                        <div className="rowitem">
                                            <div className="lbl">Mobile Networks:</div>
                                            <div className="val">{this.state.toCityResponse.networkOperator.join(", n")}</div>
                                        </div>
                                        <div className="rowitem">
                                            <div className="lbl">Beach Present:</div>
                                            <div className="val">{this.state.toCityResponse.hasBeach ? " Yes" : " No"}</div>
                                        </div>
                                        <div className="rowitem">
                                            <div className="lbl">Mountain Present:</div>
                                            <div className="val">{this.state.toCityResponse.hasMountain ? " Yes" : " No"}</div>
                                        </div>
                                        <div className="rowitem">
                                            <div className="lbl">Threat Level:</div>
                                            <div className="val">{this.state.toCityResponse.cityThreat + "%"}</div>
                                        </div>
                                        <div className="rowitem">
                                            <div className="lbl">Available Cuisines:</div>
                                            <div className="val">{this.state.toCityResponse.cuisines.join(", ")}</div>
                                        </div>
                                        <div className="rowitem">
                                            <div className="lbl">Temperature (min/max):</div>
                                            <div className="val">{this.state.toCityResponse.weatherCityParam.wt_past_1_min + " / " + this.state.toCityResponse.weatherCityParam.wt_past_1_max + " °C"}</div>
                                        </div>
                                        {
                                            this.state.toCityResponse.cityLongWeekend ? 
                                            <div className="rowitem">
                                                <div className="lbl">Long Weekend:</div>
                                                <div className="val">Yes</div>
                                            </div>
                                            :
                                            null
                                        }
                                    </div>
                                </div>
                                :
                                null
                            }
                            {
                                this.state.hotelResponse !== null ?

                                <div className="card">
                                    <div className="card-header">Hotel Details</div>
                                    <div className="card-image map">
                                    <Map center={[this.state.hotelResponse.location.lat, this.state.hotelResponse.location.lon]} zoom={12} style={{"width":"100%","height":"100%"}} scrollWheelZoom={false}>
                                        <TileLayer
                                        url="https://api.mapbox.com/styles/v1/vsvanshi/cjbgp9nfw7gra2rmo76sd8gd0/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoidnN2YW5zaGkiLCJhIjoiY2l0YnF3Yzl1MDd0YjJvczZiempmczc0NyJ9.PUqEP-Mrl2Fxl82v6jyCOA"
                                        attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"/>
                                            <Marker position={[this.state.hotelResponse.location.lat, this.state.hotelResponse.location.lon]} icon={icon}>
                                        </Marker>
                                    </Map>
                                    </div>
                                    <div className="card-body">
                                        <div className="rowitem">
                                            <div className="lbl">Hotel Name:</div>
                                            <div className="val">{this.state.hotelResponse.name}</div>
                                        </div>
                                        <div className="rowitem">
                                            <div className="lbl">Locality:</div>
                                            <div className="val">{this.state.hotelResponse.locality}</div>
                                        </div>
                                        {
                                            this.state.hotelResponse.filterLanguageSpoken.length > 0 ?
                                            <div className="rowitem">
                                                <div className="lbl">Languages Spoken:</div>
                                                <div className="val">{this.state.hotelResponse.filterLanguageSpoken.join(", ")}</div>
                                            </div>
                                            :
                                            null
                                        }
                                    </div>
                                </div>
                                :
                                null
                            }

                            {
                                this.state.hotelAmenityResponse.length > 0 ?
                                <div className="card">
                                    <div className="card-header">Hotel Amenities</div>
                                    <div className="card-body" style={{padding:"10px 0px"}}>
                                        {
                                            this.state.hotelAmenityResponse.map((amenity,index)=>{
                                                return(
                                                    <div className="am-item" key={index}>
                                                        <img className="amico" src={"http://62.210.93.54:9568/amenities/" + amenity.amenityId+".svg"}/>
                                                        <div className="amname">{amenity.amenityLabel}</div>
                                                    </div>
                                                );
                                            })
                                        }
                                    </div>
                                </div>
                                :
                                null
                            }

                            {
                                this.state.localPlacesResponse != null ?
                                <div className="card">
                                    <div className="card-header">Nearby Places</div>
                                    <div className="card-body">
                                        {
                                            this.state.localPlacesResponse.transport.airport.length > 0 ?
                                            <div style={{borderBottom:"1px solid #ccc"}}>
                                                {
                                                    this.state.localPlacesResponse.transport.airport.map((airport,index)=>{
                                                        return(
                                                            <div className="rowitem" key={index}>
                                                                <div className="lbl">Airport:</div>
                                                                <div className="val">{airport.name + " (" + airport.distance.toFixed(2) + " kms)"}</div>
                                                            </div>
                                                        );
                                                    })
                                                }
                                            </div>
                                            :
                                            null
                                        }

                                        {
                                            this.state.localPlacesResponse.transport.train.length > 0 ?
                                            <div style={{borderBottom:"1px solid #ccc"}}>
                                                {
                                                    this.state.localPlacesResponse.transport.train.map((train,index)=>{
                                                        return(
                                                            <div className="rowitem" key={index}>
                                                                <div className="lbl">Train Station:</div>
                                                                <div className="val">{train.name + " (" + train.distance.toFixed(2) + " kms)"}</div>
                                                            </div>
                                                        );
                                                    })
                                                }
                                            </div>
                                            :
                                            null
                                        }

                                        {
                                            this.state.localPlacesResponse.transport.bus.length > 0 ?
                                            <div>
                                                {
                                                    this.state.localPlacesResponse.transport.bus.map((bus,index)=>{
                                                        if(index < 3){
                                                            return(
                                                                <div className="rowitem" key={index}>
                                                                    <div className="lbl">{"Bus Stop"}:</div>
                                                                    <div className="val">{bus.name + " (" + bus.distance.toFixed(2) + " kms)"}</div>
                                                                </div>    
                                                            );
                                                        } else {
                                                            return null
                                                        }
                                                    })  
                                                }
                                            </div>
                                            :
                                            null
                                        }

                                        {
                                            this.state.localPlacesResponse.transport.subway.length > 0 ?
                                            <div>
                                                {
                                                    this.state.localPlacesResponse.transport.subway.map((subway,index)=>{
                                                        if(index < 3){
                                                            return(
                                                                <div className="rowitem" key={index}>
                                                                    <div className="lbl">{"Subway"}:</div>
                                                                    <div className="val">{subway.name + " (" + subway.distance.toFixed(2) + " kms)"}</div>
                                                                </div>    
                                                            );
                                                        } else {
                                                            return null
                                                        }
                                                    })  
                                                }
                                            </div>
                                            :
                                            null
                                        }
                                        {
                                            this.state.localPlacesResponse.peaks.result.ocean != "" ?  
                                            <div className="rowitem">
                                                <div className="lbl">Ocean:</div>
                                                <div className="val">{this.state.localPlacesResponse.peaks.result.ocean}</div>
                                            </div>
                                            :
                                            null
                                        }
                                        {
                                            this.state.localPlacesResponse.peaks.result.beach.exists ?  
                                            <div className="rowitem">
                                                <div className="lbl">Beach Present:</div>
                                                <div className="val">{"Yes" + (this.state.localPlacesResponse.peaks.result.beach.name !== "" ? " (" + this.state.localPlacesResponse.peaks.result.beach.name + ")" : "")}</div>
                                            </div>
                                            :
                                            null
                                        }
                                    </div>
                                </div>
                                :
                                null
                            }

                            {

                            }

                            {
                                this.state.inferenceParamResponse !== null ?

                                <div className="card">
                                    <div className="card-header">Inferences</div>
                                    <div className="card-image" style={{backgroundImage:"url( " + require("../images/inference.png") + ")"}}/>
                                    <div className="card-body">
                                        <div className="rowitem">
                                            <div className="lbl">Distance:</div>
                                            <div className="val">{this.state.inferenceParamResponse.distance}</div>
                                        </div>
                                        <div className="rowitem">
                                            <div className="lbl">Connectivity Concern:</div>
                                            <div className="val">{this.state.inferenceParamResponse.connectivityConcern}</div>
                                        </div>
                                        <div className="rowitem">
                                            <div className="lbl">Food Concern:</div>
                                            <div className="val">{this.state.inferenceParamResponse.foodConcern}</div>
                                        </div>
                                        <div className="rowitem">
                                            <div className="lbl">Available Networks:</div>
                                            <div className="val">{this.state.inferenceParamResponse.networkAvailable.split(",").join(", ")}</div>
                                        </div>
                                        <div className="rowitem">
                                            <div className="lbl">Traveller Type:</div>
                                            <div className="val">{this.state.inferenceParamResponse.travellerType}</div>
                                        </div>
                                        <div className="rowitem">
                                            <div className="lbl">Travel Duration:</div>
                                            <div className="val">{this.state.inferenceParamResponse.travelType}</div>
                                        </div>
                                        <div className="rowitem">
                                            <div className="lbl">Threat Concern:</div>
                                            <div className="val">{this.state.inferenceParamResponse.threatConcern}</div>
                                        </div>
                                        <div className="rowitem">
                                            <div className="lbl">Possibility of Snow:</div>
                                            <div className="val">{this.state.inferenceParamResponse.snowPossible ? "Yes" : "No"}</div>
                                        </div>
                                        {
                                            this.state.inferenceParamResponse.goingToCold ?
                                            <div className="rowitem">
                                                <div className="lbl">Going to cold place:</div>
                                                <div className="val">{"Yes"}</div>
                                            </div>
                                            :
                                            null
                                        }
                                        {
                                            this.state.inferenceParamResponse.goingToHot ?
                                            <div className="rowitem">
                                                <div className="lbl">Going to Hot place:</div>
                                                <div className="val">{"Yes"}</div>
                                            </div>
                                            :
                                            null
                                        }
                                        {
                                            this.state.inferenceParamResponse.elevationConcern ?
                                            <div className="rowitem">
                                                <div className="lbl">Elevation Concern:</div>
                                                <div className="val">{"Yes"}</div>
                                            </div>
                                            :
                                            null
                                        }
                                        {
                                            this.state.inferenceParamResponse.finalCuisines.length > 0 ?
                                            <div className="rowitem">
                                                <div className="lbl">Preferred Cuisines:</div>
                                                <div className="val">{this.state.inferenceParamResponse.finalCuisines.join(", ")}</div>
                                            </div>
                                            :
                                            null
                                        }
                                    </div>
                                </div>
                                :
                                null
                            }

                            {
                                this.state.localPlacesResponse !== null && this.state.localPlacesResponse.pois.length > 0 ?
                                <div className="card wide">
                                    <div className="card-header">Places of interest</div>
                                    <div className="card-image map">
                                    <Map 
                                        ref="map"
                                        center={[this.state.hotelResponse.location.lat, this.state.hotelResponse.location.lon]} 
                                        zoom={13} style={{"width":"100%","height":"100%"}} 
                                        scrollWheelZoom={false}>
                                        <TileLayer
                                        url="https://api.mapbox.com/styles/v1/vsvanshi/cjbgp9nfw7gra2rmo76sd8gd0/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoidnN2YW5zaGkiLCJhIjoiY2l0YnF3Yzl1MDd0YjJvczZiempmczc0NyJ9.PUqEP-Mrl2Fxl82v6jyCOA"
                                        attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"/>
                                        {
                                            this.state.localPlacesResponse.pois.map((_poi,index)=>{
                                                var markerHtml = "<div class='circle-marker' style='background-color: " + _poi.color + "'></div>";
                                                var iconToShow = divIcon({
                                                    className: 'location-marker',
                                                    html:markerHtml});
                                                return(
                                                    <Marker ref={"marker_" + index} key={index} position={[_poi.location.lat, _poi.location.lon]} icon={iconToShow}>
                                                        <Popup>
                                                            <div>
                                                            {_poi.name}
                                                            </div>
                                                        </Popup>
                                                    </Marker>
                                                );
                                            })
                                        }
                                        <Marker ref="hotel" position={[this.state.hotelResponse.location.lat, this.state.hotelResponse.location.lon]} icon={icon}>
                                            <Popup><div>Your Hotel</div></Popup>
                                        </Marker>
                                    </Map>
                                    </div>
                                    <div className="card-body">
                                        <div className="places">
                                            <div className="poi">
                                                <div className="hotel-marker1"/>
                                                <div className="poit">Your Hotel</div>
                                            </div>
                                            {
                                                this.state.placesOfInterest.map((poi,index)=>{
                                                    return(
                                                        <div className="poi" key={index}>
                                                            <div className="circle-marker" style={{backgroundColor:poi.color}}/>
                                                            <div className="poit">
                                                            {
                                                                poi.type == "triprestaurant" ? "Restaurant" : poi.type
                                                            }</div>
                                                        </div>
                                                    )
                                                })
                                            }
                                        </div>
                                    </div>
                                </div>
                                :
                                null
                            }
                        </div>
                        :
                        <div className="info-msg">Start by putting your details on the left and hit search</div>
                    }
                </div>
            </div>
        );
    }
}