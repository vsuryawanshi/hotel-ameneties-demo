import React, { Component } from 'react';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import DatePicker from 'react-datepicker';
import moment from 'moment';
 
import 'react-datepicker/dist/react-datepicker.css';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';


export default class ProfilePage extends Component {
    constructor(props){
        super(props);
        this.state = {
            duration : 0,
            currentDate : moment(),
            radioValue:""
        }
    }
    handleChange = event => {
        this.setState({ duration: event.target.value });
    };

    handleDateChange = date => {
        this.setState({ currentDate: date })
    }

    handleRadioChange = (value) => {
        this.setState({ radioValue:value });
      };
    render() {
        return (
            <div className="user-profile">
                <h1 className="heading">User Profile</h1>
                <div className="woocard user">
                    <div className="user-section">
                        <div className="user-image" style={{"background":"url(" + require("../images/profile/varun.jpg") +")", "backgroundSize":"cover","backgroundPosition":"center"}}/>
                        <div className="user-info">
                            <div className="user-name">Varun Suryawanshi</div>
                            <div className="coin-type">ETHEREUM</div>
                        </div>
                    </div>
                    <div className="user-level" style={{"background":"url(" + require("../images/profile/bitcoin.jpg") +")", "backgroundSize":"cover","backgroundPosition":"center"}}>
                        <div className="level">Amateur</div>
                    </div>
                    <div className="user-stats">
                        <div className="stat">
                            <div className="num">10,000</div>
                            <div className="extra">(Total Coins)</div>
                        </div>
                        <div className="stat">
                            <div className="num">12</div>
                            <div className="extra">(Rank in friends)</div>
                        </div>
                        <div className="stat">
                            <div className="num">77,333</div>
                            <div className="extra">(Rank in country)</div>
                        </div>
                        <div className="stat">
                            <div className="num">33 million</div>
                            <div className="extra">(Rank worldwide)</div>
                        </div>
                    </div>
                </div>

                <div className="woocard stats">
                    <div className="stat-header">
                        Statistics
                    </div>
                    <div className="filter-wrap">
                        <div className="filter-inputs">
                            <div className={`finput` + (this.state.duration == 0  ? " selected" : "")} onClick={(event)=>{
                                this.setState({
                                    duration:0
                                })
                            }}>
                                Today
                            </div>
                            <div className={`finput` + (this.state.duration == 1  ? " selected" : "")} onClick={(event)=>{
                                this.setState({
                                    duration:1
                                })
                            }}>
                                Last week
                            </div>
                        </div>
                    </div>
                    <div className="list-wrap">
                        <div className="list-item">
                            <div className="top-wrap">
                                <img className="coin-image" src={require("../images/profile/bitcoinlogo.png")}/>
                                <div className="info">
                                    <div className="coin-name">
                                        Bitcoin
                                    </div>
                                    <div className="desc">
                                        You earned $129
                                    </div>
                                </div>
                                <div className="diff positive">
                                    + $129
                                </div>
                            </div>
                            <div className="actions-wrap">
                                <FlatButton label="Buy" primary={true} />
                                <FlatButton label="Sell" secondary={true} />
                            </div>
                        </div>
                        <div className="list-item fadeout">
                            <div className="top-wrap">
                                <img className="coin-image" src={require("../images/profile/ripplelogo.png")}/>
                                <div className="info">
                                    <div className="coin-name">
                                        Ripple
                                    </div>
                                    <div className="desc">
                                        You lost $95
                                    </div>
                                </div>
                                <div className="diff negative">
                                    - $95
                                </div>
                            </div>
                            <div className="actions-wrap">
                                <FlatButton label="Buy" primary={true} />
                                <FlatButton label="Sell" secondary={true} />
                            </div>
                        </div>
                        <div className="list-item">
                            <div className="top-wrap">
                                <img className="coin-image" src={require("../images/profile/ethereumlogo.png")}/>
                                <div className="info">
                                    <div className="coin-name">
                                        Ethereum
                                    </div>
                                    <div className="desc">
                                        You earned $15
                                    </div>
                                </div>
                                <div className="diff positive">
                                    + $15
                                </div>
                            </div>
                            <div className="actions-wrap">
                                <FlatButton label="Buy" primary={true} />
                                <FlatButton label="Sell" secondary={true} />
                            </div>
                        </div>
                    </div>
                    <div className="total-wrap">
                        Your total earnings : <span className="total positive"> + $49</span>
                    </div>
                </div>
                <div className="woocard bounty">
                    <div className="bounty-header">
                        Bounty Available
                    </div>
                    <div className="question-wrap">
                        Who is the holder of the highest number of bitcoins as of this week?
                    </div>
                    <div className="reward-wrap">
                        <img src={require("../images/profile/coins.svg")} className="reward-image"/>
                        <div className="reward-text">250 MinaCoins</div>
                    </div>
                    <div className="answer-wrap">
                        <div className="form-comp">
                            <input type="text" className="woo-text" placeholder="Your answer text"/>
                        </div>
                        <div className="form-comp">
                            <DatePicker
                                selected={this.state.currentDate}
                                onChange={this.handleDateChange}
                                isClearable={true}
                                placeholderText="Select a date"
                                className="woo-text"/>    
                        </div>
                        <div className="form-comp">
                            <RadioButtonGroup name="answerValue" defaultSelected="not_light">
                                <RadioButton
                                    value="light"
                                    label="Answer 1"
                                />
                                <RadioButton
                                    value="not_light"
                                    label="Answer 2"
                                />
                            </RadioButtonGroup>
                        </div>
                    </div>
                    <div className="submit-wrap">
                        <RaisedButton label="Submit" primary={true} />
                    </div>
                </div>
            </div>
        );
    }
}