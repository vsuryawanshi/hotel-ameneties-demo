import React, {Component} from 'react';
import VerticalCarousal from "../components/VerticalCarousal";
import HeaderComponent from "../components/Home/Header";
import LandingComponent from "../components/Home/Landing";
import InfoComponent from "../components/Home/Info";
import ContactUs from "../components/Home/Contact";
import MinaTechComp from "../components/Home/MinaTech";
import Enticer from "../components/Home/Enticer";
import "leaflet/dist/leaflet.css";

export default class Home extends Component {
    handleMenuItemClick(indexToScrollTo){
        this.refs.vcarousel.goToPosition(indexToScrollTo);
    }

    handleSectionLoadStart(sectionNumber){
        if(sectionNumber != 2){
            this.refs.minacomp.resetSecondPageAnimations();
        }
    }

    handleSectionLoaded(loadedSectionNumber){
        this.refs.header.setCurrentSection(loadedSectionNumber);
        switch(loadedSectionNumber){
            case 0:
                this.refs.minacomp.resetSecondPageAnimations();
                this.refs.enticer.showProductInfo();
                break;
            case 1:
                this.refs.enticer.showProductInfo();
                this.refs.minacomp.resetSecondPageAnimations();
                break;
            case 2:
                this.refs.minacomp.startSecondPageAnimations();
                this.refs.enticer.showProductInfo();
                break;
            default:
                this.refs.minacomp.resetSecondPageAnimations();
                this.refs.enticer.showProductInfo();
                break;
        }
    }

    render() {
        return (
            <div className="homepage-wrapper">
                <HeaderComponent clickedMenuItem={this.handleMenuItemClick.bind(this)} ref="header" {...this.props}/>
                <VerticalCarousal
                    ref="vcarousel"
                    sectionLoadStarted={this.handleSectionLoadStart.bind(this)}
                    sectionLoaded={this.handleSectionLoaded.bind(this)}>
                    <div className="info-section" style={{"backgroundColor":"#17abf5"}}>
                        <LandingComponent/>   
                    </div>  
                    <div className="info-section" id="product" style={{"backgroundColor":"#04b7a8"}}>
                        <Enticer ref="enticer"/>       
                    </div>
                    <div className="info-section" style={{"backgroundColor":"#f76190"}}>
                        <MinaTechComp ref="minacomp"/>    
                    </div>  
                    <div className="info-section" style={{"backgroundColor":"#5c83f7"}}>
                        <InfoComponent/>   
                    </div>  
                    <div className="info-section" style={{"backgroundColor":"#17abf5"}}>
                        <ContactUs/>   
                    </div>  
                </VerticalCarousal>
            </div>
        );
    }
}