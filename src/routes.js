import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './containers/App';
import Demopage from './containers/DemoPage';
import NotFoundPage from './containers/NotFoundPage.js';

export default (
    <Route>
        <Route path="/" component={App}>
            <IndexRoute component={Demopage} />
            <Route path="/demo" component={Demopage}/>
            <Route path="*" component={NotFoundPage}/>
        </Route>
  </Route>
);
