import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { MENU_ITEMS} from "../../common/constants";
import TextAnimate from "../../components/TextAnimate";

export default class HeaderComponent extends Component {
    constructor(props){
        super(props);
        this.state = {
            currentVisibleSection:-1
        };
    }
    setCurrentSection(sectionNumber){
        this.setState({currentVisibleSection : sectionNumber});
    }
    componentWillUnmount() {
        this.refs.taHead.stopComponent();
    }
    render() {
        return (
            <header className="woo-header">
                <div className="container">
                    <div className="logo" onClick={(event)=>{
                        event.preventDefault();
                        event.stopPropagation();
                        this.props.clickedMenuItem(0);
                    }}>
                        <div className="mainh">mina</div>
                        <div className="tagl">
                            <TextAnimate items={["Think like humans","Work like machines"]} timeout={3000} ref="taHead"/>
                        </div>
                    </div>
                    <div className="menu-wrap">
                        {
                            MENU_ITEMS.map((menu_item) => {
                                if(menu_item.type == "internal"){
                                    return(
                                        <div key={menu_item.index} className={`menu-item` + (this.state.currentVisibleSection == menu_item.index ? " active" : "")}
                                            onClick={(event)=>{
                                                    event.preventDefault();
                                                    event.stopPropagation();
                                                    this.props.clickedMenuItem(menu_item.index);
                                                }}>
                                            {menu_item.name}
                                        </div>
                                    );
                                } else if(menu_item.type == "button") {
                                    return (
                                        <div className="menu-item" key={menu_item.index} style={{"display":"none"}}>
                                            <button className="woo-btn small" onClick={(event)=>{
                                                event.stopPropagation();
                                                event.preventDefault();
                                                this.props.router.push("demo");
                                            }}>{menu_item.name}</button>
                                        </div>
                                    );
                                }
                            })
                        }
                    </div>
                </div>
            </header>
        );
    }
}

HeaderComponent.propTypes = {
    clickedMenuItem : PropTypes.func,
    router:PropTypes.object
};