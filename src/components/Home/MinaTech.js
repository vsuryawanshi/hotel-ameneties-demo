import React, { Component } from 'react';
import ReactDOM from "react-dom";
import {FIRST_ANIM, SECOND_ANIM, THIRD_ANIM, FOURTH_ANIM} from "../../common/constants";

export default class MinaTechComp extends Component {
    constructor(props){
        super(props);
        this.state = {
            chips :[],
            animateText:"Mina’s cutting-edge computer vision capabilities analyzes hotel images through hundreds of different parameters, promising to present the right picture in the right context."
        };
    }
    extractAndShowChips(arr, callback){
        var loopIndex = 1;
        this.cinterval = window.setInterval(()=>{
            this.setState({chips:[]});
            var slicedArray = arr.slice(0,loopIndex++);
            this.setState({chips:slicedArray});
            if(loopIndex == arr.length + 1){
                clearInterval(this.cinterval);
                setTimeout(()=>{
                    callback();
                },2000);
            }
        },1000);   
    }

    startSecondPageAnimations(){
        var elem = ReactDOM.findDOMNode(this.refs.anim1);
        elem.style.display = "block";
        this.setState({animateText:"Mina’s cutting-edge computer vision capabilities analyzes hotel images through hundreds of different parameters, promising to present the right picture in the right context."})
        this.ct = setTimeout(()=>{
            ReactDOM.findDOMNode(this.refs.zoomlens).classList.add("path1");
            var topPosition = $(elem).position().top + $(elem).height() + 70;
            $(".chips-wrap").css({top:topPosition+"px"});
            this.extractAndShowChips(FIRST_ANIM,()=>{
                this.setState({chips:[],animateText:"Our built-in natural-language processing engine gives her the capability to understand every review and user sentiments in minute detail."});
                this.moveToSecondImage();
            });
        },1000);
    }

    moveToSecondImage(){
        var a1 = ReactDOM.findDOMNode(this.refs.anim1);
        a1.style.display = "none";
        var a2 = ReactDOM.findDOMNode(this.refs.anim2)
        a2.style.display = "flex";
        this.ct = setTimeout(()=>{
            ReactDOM.findDOMNode(this.refs.zoomlens1).classList.add("path2");
            var topPosition = $(a2).position().top + $(a2).height() + 70;
            $(".chips-wrap").css({top:topPosition+"px"});
            this.extractAndShowChips(SECOND_ANIM,()=>{
                this.setState({
                    chips:[],
                    animateText:"Mina’s operating system understands different personas and meets their personalized needs. She provides a one-to-one conversational experience with every individual, no matter their cultural background."
                });
                this.moveToThirdImage();
            });
        },1000)
    }

    moveToThirdImage(){
        var a2 = ReactDOM.findDOMNode(this.refs.anim2);
        a2.style.display = "none";
        var a3 = ReactDOM.findDOMNode(this.refs.anim3);
        a3.style.display = "block";
        this.ct = setTimeout(()=>{ 
            ReactDOM.findDOMNode(this.refs.zoomlens2).classList.add("path3");
            var topPosition = $(a3).position().top + $(a3).height() + 70;
            $(".chips-wrap").css({top:topPosition+"px"});
            this.extractAndShowChips(THIRD_ANIM, ()=>{
                this.setState({
                    chips:[],
                    animateText:"Our global location intelligence engine understands the DNA of every location on earth and is used in personalizing every message sent to show them features unavailable at their current location."
                });
                this.moveToFourthImage();
            });
        },1000)
    }

    moveToFourthImage(){
        ReactDOM.findDOMNode(this.refs.anim3).style.display = "none"; 
        var a4 = ReactDOM.findDOMNode(this.refs.anim4);
        a4.style.display = "block"; 
        this.ct = setTimeout(()=>{ 
            ReactDOM.findDOMNode(this.refs.zoomlens3).classList.add("path2");
            var topPosition = $(a4).position().top + $(a4).height() + 70;
            $(".chips-wrap").css({top:topPosition+"px"});
            this.extractAndShowChips(FOURTH_ANIM, ()=>{
                this.resetSecondPageAnimations();
                this.startSecondPageAnimations();
            });
        },1000);
    }

    resetSecondPageAnimations(){
        this.refs.zoomlens.removeAttribute("class");
        this.refs.zoomlens.classList.add("zoomlens");
        ReactDOM.findDOMNode(this.refs.anim2).style.display = "none";
        ReactDOM.findDOMNode(this.refs.anim3).style.display = "none";
        ReactDOM.findDOMNode(this.refs.anim4).style.display = "none";
        this.setState({animateText:""})
        this.setState({chips:[]});
        window.clearTimeout(this.ct);
        window.clearInterval(this.cinterval);
    }
    render() {
        return (
            <div className="container">
            <div className="texte small">
                {this.state.animateText}
            </div>
            <div className="post-card first right" ref="anim1">
                <div ref="bgimage" className="img-holder" style={{"backgroundImage":"url(" + require("../../images/3.jpg") + ")"}}/>
                <div className="zoomlens" ref="zoomlens"/>
            </div>
            <div className="normal-text" ref="anim2" style={{"display":"none"}}>
                <div className="zoomlens dark" ref="zoomlens1"/>
                <div className="review-page">
                    <div className="blue-title"/>
                    <div className="content-wrap">
                        <div className="content-pic">
                            <img src={require("../../images/review-pic.png")}/>
                        </div>
                        <div className="content-text">
                            <div className="cheading">
                                Dirty room...unsafe outdoors, really bad experience!    
                            </div>
                            <p>Wow, I've never needed a drink so badly and it is illegal there.</p>
                            <p>The area is horrific, literally a rubbish dump packed with many shady characters on motorbikes hanging around in packs.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="t-img" ref="anim3" style={{"display":"none"}}>
                <div className="zoomlens dark" ref="zoomlens2"/>
                    <img src={require("../../images/old-man-standing.png")}/>
                    <img src={require("../../images/old-woman-standing.png")}/>
            </div>
            <div className="post-card first right" ref="anim4" style={{"display":"none"}}>
                <div className="zoomlens" ref="zoomlens3"/>
                <div ref="bgimage" className="img-holder" style={{"backgroundImage":"url(http://www.thehindu.com/news/cities/Delhi/article20004885.ece/alternates/FREE_660/DELHISMOGPOLLUTION)"}}/>
            </div>
            <div className="chips-wrap">
            {
                this.state.chips.map((chipItem, index)=>{
                    return(
                        <div className="chip" key={index}>
                            <div className="ctext">{chipItem.text}</div>
                            <div className="cicon">
                                <img src={chipItem.image}/>
                            </div>
                            <div className="cpercent">{chipItem.percent}</div>
                        </div>
                    );
                })
            }
            </div>
        </div>
        );
    }
}