import React, { Component } from 'react';
import {HOME_LOCATION} from "../../common/constants";
import { divIcon} from 'leaflet';
import { Map, Marker, TileLayer } from 'react-leaflet';

export default class ContactUs extends Component {
    constructor(props){
        super(props);
        this.locationIcon = divIcon({
            className: 'location-marker',
            iconAnchor:[20,40],
            html:"<div class='main_map-marker'></div>"});
    }
    render() {
        return (
            <div className="about-wrap">
                <div className="map-wrap">
                    <Map center={HOME_LOCATION} zoom={5} style={{"width":"100%","height":"100%"}} scrollWheelZoom={false}>
                        <TileLayer
                        url="https://api.mapbox.com/styles/v1/vsvanshi/cjbgp9nfw7gra2rmo76sd8gd0/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoidnN2YW5zaGkiLCJhIjoiY2l0YnF3Yzl1MDd0YjJvczZiempmczc0NyJ9.PUqEP-Mrl2Fxl82v6jyCOA"
                        attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"/>
                            <Marker position={HOME_LOCATION} icon={this.locationIcon}>
                        </Marker>
                    </Map>
                </div>
                <div className="info-wrap">
                    <div className="add-wrap">
                        <div className="main-logo">
                                mina
                            </div>
                        <div className="address">
                            <img src={require("../../images/placeholder.svg")}/>
                                Kasavanahalli
                                <br/>
                                Bangalore, Karnataka, IND
                            </div>
                        <div className="address">
                            <img src={require("../../images/phone.svg")}/>
                                +91-9739109397
                        </div>
                        <div className="address">
                            <img src={require("../../images/mail.svg")}/>
                            <a className="mailink" href="mailto:contact@mina.global">contact@mina.global </a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}