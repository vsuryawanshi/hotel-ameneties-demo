import React from "react";

const InfoComponent = () =>
<div className="team-wrapper">
    <div className="about-heading">
        The people behind Mina
    </div>
    <div className="about-subhead">
        We are a young and hungry tech venture bringing new technologies and integrative solutions to the digital space. Our goal is to transform hotels into hospitality heroes and website visitors into happy customers.
    </div>
    <div className="sec-one">
        <div className="about-sec">
            <img src={require("../../images/team.svg")} className="about-img"/>
            <div className="sec-desc">
                10+ dedicated people   
            </div>     
        </div>
        <div className="about-sec">
            <img src={require("../../images/world1.svg")} className="about-img"/>
            <div className="sec-desc">
                Europe, Asia and the US
            </div>     
        </div>
    </div>
    <div className="sec-one">
        <div className="about-sec">
            <img src={require("../../images/innovate.svg")} className="about-img"/>
            <div className="sec-desc">
                Passion for technology and innovation
            </div>     
        </div>
        <div className="about-sec">
            <img src={require("../../images/travel.svg")} className="about-img"/>
            <div className="sec-desc">
                Focus on travel and hospitality
            </div>     
        </div>
    </div>
</div>

export default InfoComponent;