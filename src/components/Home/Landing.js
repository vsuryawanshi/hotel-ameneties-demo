import React from "react";
const LandingComponent = () => 
<div className="landing-wrap">
    <div className="text">
        Meet Mina, the hospitality industry’s first humanoid to supercharge booking conversions, powered by DNAfication
    </div>
    <div className="imag">
        <img src={require("../../images/support.png")}/>
    </div>
</div>

export default LandingComponent;