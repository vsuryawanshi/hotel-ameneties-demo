import React, {Component} from "react";
import ReactDOM from "react-dom";

export default class ScratchCard extends Component{
    constructor(props){
        super(props);
        this.state = {
            loaded : false,
            flashOffer:false
        }
    }

    componentDidMount() {
        this.offerCheckInterval = setInterval(()=>{
            var now = new Date();
            var endTime = new Date(this.props.inputData.content.offer.endTime);
            var diffInSecs = Math.floor((endTime-now)/1000);
            var thresholdDiffInSecs = 0;
            console.log(diffInSecs + " s");
            if(this.props.inputData.content.offer.threshold.unit.toLowerCase() == "mins"){
                thresholdDiffInSecs = parseInt(this.props.inputData.content.offer.threshold.value) * 60;
            } else if(this.props.inputData.content.offer.threshold.unit.toLowerCase() == "secs"){
                thresholdDiffInSecs = parseInt(this.props.inputData.content.offer.threshold.value);
            }
            if(diffInSecs <= thresholdDiffInSecs){
                this.triggerOfferEnding(thresholdDiffInSecs);
                clearInterval(this.offerCheckInterval);
            }
            
        },1000);
        if(this.props.inputData.scratchOptions.active){
            this.isDrawing = false;
            this.lastPoint = null;
            this.ctx = this.canvas.getContext('2d');
        
            const image = new Image();
            image.crossOrigin = "Anonymous";
            image.onload = () => {
              this.ctx.drawImage(image, 0, 0, image.width, image.height, 0, 0 , this.canvas.width, this.canvas.height);
              this.setState({ loaded: true });
            }
            image.src = this.props.inputData.scratchOptions.scratchOverlayImage;
        } else{
            this.setState({ loaded: true });
        }
    }

    triggerOfferEnding(secsRemaining){
        console.log("offer about to end")
        if(this.props.inputData.content.offer.endType.toLowerCase() == "flash"){
            this.setState({flashOffer:true})
        } else if(this.props.inputData.content.offer.endType.toLowerCase() == "fadeout"){
            var container = ReactDOM.findDOMNode(this.refs.widgetContainer);
            container.style.animation = "widgetFadeOut " + secsRemaining + "s" + " linear"; 
            setTimeout(() => {
                container.style.display = "none";
            }, secsRemaining*1000);
        }
    }
    
    componentWillReceiveProps(nextProps) {
        if(this.props.inputData.scratchOptions.active){
            this.isDrawing = false;
            this.lastPoint = null;
            this.ctx = this.canvas.getContext('2d');
        
            const image = new Image();
            image.crossOrigin = "Anonymous";
            image.onload = () => {
              this.ctx.drawImage(image, 0, 0, image.width, image.height, 0, 0 , this.canvas.width, this.canvas.height);
              this.setState({ loaded: true });
            }
            image.src = nextProps.inputData.image;    
        }
    }
    
    getFilledInPixels(stride) {
        if (!stride || stride < 1) {
          stride = 1;
        }
    
        const pixels = this.ctx.getImageData(0, 0, this.canvas.width, this.canvas.height);
        const total = pixels.data.length / stride;
        let count = 0;
    
        for (let i = 0; i < pixels.data.length; i += stride) {
          if (parseInt(pixels.data[i], 10) === 0) {
            count++;
          }
        }
    
        return Math.round((count / total) * 100);
    }
    
    getMouse(e, canvas) {
        const {top, left} = canvas.getBoundingClientRect();
        const scrollTop  = window.pageYOffset || document.documentElement.scrollTop;
        const scrollLeft = window.pageXOffset || document.documentElement.scrollLeft;
    
        return {
            x: (e.pageX || e.touches[0].clientX) - left - scrollLeft,
            y: (e.pageY || e.touches[0].clientY) - top - scrollTop
        }
    }
    
    distanceBetween(point1, point2) {
        return Math.sqrt(
          Math.pow(point2.x - point1.x, 2) + Math.pow(point2.y - point1.y, 2)
        );
    }
    
    angleBetween(point1, point2) {
        return Math.atan2(point2.x - point1.x, point2.y - point1.y);
    }
    
    handlePercentage(filledInPixels = 0) {
        if (filledInPixels > this.props.finishPercent) {
          this.canvas.parentNode.removeChild(this.canvas);
          this.setState({ finished: true });
          if (this.props.onComplete) {
            this.props.onComplete();
          }
        }
    }
    
    handleMouseDown(e) {
        this.isDrawing = true;
        this.lastPoint = this.getMouse(e, this.canvas);
    }
    
    handleMouseMove(e) {
        if (!this.isDrawing) {
          return;
        }
    
        e.preventDefault();
    
        const currentPoint = this.getMouse(e, this.canvas);
        const distance = this.distanceBetween(this.lastPoint, currentPoint);
        const angle = this.angleBetween(this.lastPoint, currentPoint);
    
        let x, y;
    
        for (let i = 0; i < distance; i++) {
          x = this.lastPoint.x + (Math.sin(angle) * i);
          y = this.lastPoint.y + (Math.cos(angle) * i);
          this.ctx.globalCompositeOperation = 'destination-out';
          this.ctx.beginPath();
          this.ctx.arc(x, y, 25, 0, 2 * Math.PI, false);
          this.ctx.fill();
        }
    
        this.lastPoint = currentPoint;
        this.handlePercentage(this.getFilledInPixels(32));
    
    }
    
    handleMouseUp() {
        this.isDrawing = false;
    }

    render(){
        const containerStyle = {
            width: this.props.inputData.width || "200px",
            height: this.props.inputData.height || "200px",
            position: 'fixed',
            top:this.props.inputData.widget_position.top,
            left:this.props.inputData.widget_position.left,
            WebkitUserSelect: 'none',
            MozUserSelect: 'none',
            msUserSelect: 'none',
            userSelect: 'none'
          }
      
          const canvasStyle = {
            position: 'absolute',
            top: 0,
            zIndex: 1000000
          }
      
          const resultStyle = {
            visibility: this.state.loaded ? 'visible' : 'hidden',
            height:"100%"
          }
      
          const canvasProps = {
            ref: (ref) => this.canvas = ref,
            className: 'ScratchCard__Canvas',
            style: canvasStyle,
            width: this.props.inputData.width,
            height:this.props.inputData.height,
            onMouseDown: this.handleMouseDown.bind(this),
            onTouchStart: this.handleMouseDown.bind(this),
            onMouseMove: this.handleMouseMove.bind(this),
            onTouchMove: this.handleMouseMove.bind(this),
            onMouseUp: this.handleMouseUp.bind(this),
            onTouchEnd: this.handleMouseUp.bind(this)
          }
        let backgroundImageStyles = {};
        if(this.props.inputData.content.backgroundImage){
            backgroundImageStyles = {
                "backgroundImage":"url(" + this.props.inputData.content.backgroundImage + ")",
                "backgroundSize":"cover",
                "backgroundPosition":"center",
                "height":"100%"
            }
        }
        return(
            <div className="wgt__sratch__wrapper" style={containerStyle} ref="widgetContainer">
                {
                    this.props.inputData.scratchOptions.active ?
                    <canvas {...canvasProps}></canvas>
                    :
                    null
                }
                <div className="ScratchCard__Result" style={resultStyle}>
                    <div className="wgt__gift__wrap" style={backgroundImageStyles}>
                        <div className="wgt__gift__mask" />
                        <div className="wgt__offer__content">
                            <div className="wgt__greeting">
                                <div className="greet__msg" style={this.props.inputData.content.greeting.styles}>
                                    {this.props.inputData.content.greeting.text}
                                </div>
                                <div className="visitor__name" style={this.props.inputData.content.main_text.styles}>
                                    {this.props.inputData.content.main_text.text}
                                </div>
                            </div>
                            <div className="offer__description" style={this.props.inputData.content.offer.main.styles}>
                                {this.props.inputData.content.offer.main.text}
                            </div>
                            <div className="wgt__footer__card">
                                <div className={`offer__validity` + (this.state.flashOffer ? " flash" : "")}>
                                    <img src="http://62.210.93.54:8888/images/widget/schedule-button.svg"/>
                                    <div className={`offer__text`}>
                                        {this.props.inputData.content.offer.main.text}
                                    </div>
                                    
                                </div>
                                <div className="book__offer">
                                    {
                                        this.props.inputData.content.actions.map((cta,index)=>{
                                            return(
                                                <button className="wgt__btn__woo" key={index} style={cta.styles}>
                                                    {cta.text}
                                                </button>
                                            );
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}