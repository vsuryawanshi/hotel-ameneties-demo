import React, { Component } from 'react';
import { divIcon} from 'leaflet';
import { Map, Marker, Popup, TileLayer } from 'react-leaflet';

export default class LocationWidget extends Component {
    constructor(props){
        super(props);

    }
    render() {
        var markerHtml = "<div class='circle-marker'></div>";
        var icon = divIcon({
            className: 'location-marker',
            html:markerHtml});
        let wrapperStyles = {
            width:this.props.inputData.width,
            height:this.props.inputData.height,
            position:"fixed",
            zIndex:this.props.inputData.zindex,
            top:this.props.inputData.widget_position.top,
            left:this.props.inputData.widget_position.left,
            transform:"translate(-" + this.props.inputData.widget_position.left + ", -" + this.props.inputData.widget_position.top+")"
        }
        return (
            <div className="location__widget__wrapper" style={wrapperStyles}>
                <div className="location__widget__mask">
                    <div className="location__top__section">
                        <div className="location__search__section">
                            <input className="search__input" type="text" placeholder="Search a new place"/>
                        </div>
                        <div className="money__section">
                            45$
                        </div>
                    </div>
                    <div className="location__text__section">
                        <div className="location__greeting">
                            <div className="actual__greet">
                                Hola
                            </div>
                            <div className="actual__name">
                                Marcus,
                            </div>
                        </div>
                        <div className="location__question">
                            Where are you going today?
                        </div>
                        <div className="location__ans__wrap">
                            <div className="__answer__item selected">
                                Work
                            </div> 
                            <div className="__answer__item">
                                Mom's Home
                            </div> 
                            <div className="__answer__item">
                                Home
                            </div> 
                            <div className="__answer__item">
                                Play
                            </div> 
                        </div>
                    </div>
                    <div className="location__map__section">
                        <Map center={[12.939700, 77.639993]} zoom={12} style={{"width":"100%","height":"100%"}} scrollWheelZoom={false}>
                            <TileLayer
                            url="https://api.mapbox.com/styles/v1/vsvanshi/cjbgp9nfw7gra2rmo76sd8gd0/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoidnN2YW5zaGkiLCJhIjoiY2l0YnF3Yzl1MDd0YjJvczZiempmczc0NyJ9.PUqEP-Mrl2Fxl82v6jyCOA"
                            attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"/>
                                <Marker position={[12.939700, 77.639993]} icon={icon}>
                            </Marker>
                        </Map>
                    </div>
                </div>
            </div>
        );
    }
}