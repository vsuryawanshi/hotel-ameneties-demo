import React, { Component } from 'react';
import PropTypes from "prop-types";
import { divIcon,featureGroup,popup} from 'leaflet';
import { Map, Marker, Popup, TileLayer } from 'react-leaflet';
import axios from "axios";
import LoaderComponent from '../components/Common/LoaderComponent';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

const HOTEL_ICON = divIcon({className: 'location-marker',html:"<div class='hotel-marker'></div>",iconAnchor:[30,30]});
const GREEN_CIRCLE_ICON = divIcon({className: 'location-marker',html:"<div class='circle-marker'></div>",iconAnchor:[15,15]});
const MOUNTAIN_ICON = divIcon({className: 'location-marker',html:"<div class='mountain-marker'></div>",iconAnchor:[7,7]});
const PEAK_ICON = divIcon({className: 'location-marker',html:"<div class='peak-marker'></div>",iconAnchor:[7,7]});
const GLACIER_ICON = divIcon({className: 'location-marker',html:"<div class='glacier-marker'></div>",iconAnchor:[10,10]});
const VOLCANO_ICON = divIcon({className: 'location-marker',html:"<div class='volcano-marker'></div>",iconAnchor:[10,10]});

const LANGUAGES_1 = [
    {
        "name":"Hindi"
    },
    {
        "name":"English"
    }
];
const LANGUAGES_2 = [
    {
        "name":"Punjabi"
    },
    {
        "name":"Kannada"
    },
    {
        "name":"Telugu"
    }
];

export default class LocationWidgetNearby extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading:false,
            searchEnabled:true,
            errorState:false,
            errorMsg:"",
            selectedCategory:"",
            currentCategory : null,
            markerData:[],
            nearbyData:{},
            currentMarkers:[],
            selectedLanguageIndex:0,
            selectedDropdownLanguageIndex:0,
            selectedDropdownLanguage:""
        }
        this.hotelCoordinates = [32.645104, 76.091426];
        this.distanceThreshold = 1000;
    }

    componentDidMount() {
        //this.enableGoogleAutocomplete();    
        this.getNearbyData();
    }

    enableGoogleAutocomplete(){
        setTimeout(()=>{
            var defaultBounds = new google.maps.LatLngBounds(
                new google.maps.LatLng(-90,-180),
                new google.maps.LatLng(90,180));
            var input = document.getElementById('autocomp');
            var options = {
                bounds: defaultBounds,
                types: []
            };
            
            this.autocomplete = new google.maps.places.Autocomplete(input, options);
            this.autocomplete.addListener('place_changed', this.onPlaceChanged.bind(this));
        },1000);
    }

    onPlaceChanged(){
        var place = this.autocomplete.getPlace();
        this.hotelName = place.name || "";
        if(place.geometry){
            this.hotelCoordinates = [place.geometry.location.lat(), place.geometry.location.lng()];   
            this.getNearbyData(); 
        }
    }

    getNearbyData(){
        this.setState({loading:true,searchEnabled:false});
        axios.get("http://62.210.93.54:7020/coast?lat=" + this.hotelCoordinates[0] + "&lon=" + this.hotelCoordinates[1] + "&distance=" + this.distanceThreshold).then((response)=>{    
            this.parseApiResponse(response);   
        }).catch((error)=>{
            this.setState({
                loading:false,
                errorState:true,
                errorMsg:error
            })
        });
    }

    parseApiResponse(response){
        let resultObj = {};
            if(response.data.result.coast.distance != ""){
                resultObj["coast"] = response.data.result.coast;
                resultObj["coast"]["beach"] = response.data.result.beach;
                resultObj["coast"]["ocean"] = response.data.result.ocean;
            }   
            if(response.data.result.river.distance !== ""){
                resultObj["river"] = response.data.result.river;
            }
            //the peaks data has to be seggregated into different categories
            var glaciers = [], mountains = [], peaks = [], volcanoes = [];
            for(var idx in response.data.result.peaks){
                let currentPeakObj = response.data.result.peaks[idx];
                switch(currentPeakObj.geotype){
                    case "mountain":
                        mountains.push(currentPeakObj);
                        break;
                    case "peak":
                        peaks.push(currentPeakObj);
                        break;
                    case "glacier":
                        glaciers.push(currentPeakObj);
                        break;
                    case "volcano":
                        volcanoes.push(currentPeakObj);
                        break;
                }
            }
            if(glaciers.length !== 0){
                resultObj["glaciers"] = glaciers;
            }
            if(volcanoes.length !== 0){
                resultObj["volcanoes"] = volcanoes;
            }
            if(peaks.length !== 0){
                resultObj["peaks"] = peaks;
            }
            if(mountains.length !== 0){
                resultObj["mountains"] = mountains;
            }
            if(response.data.result.vegetation.score !== undefined){
                resultObj["vegetation"] = response.data.result.vegetation;
            }
            this.setState({loading:false, nearbyData:resultObj,currentCategory:null,searchEnabled:false});
    }

    handleCategoryClick(keyName,currentCategory){
        if(this.state.selectedCategory == keyName){
            this.setState({selectedCategory:"",currentCategory:null,currentMarkers:[]});
        } else {
            switch(keyName){
                case "coast":
                case "river":
                    if(currentCategory.lat != "" && currentCategory.lng != ""){
                        let marker ={};
                        marker["pos"] = [parseFloat(currentCategory.lat), parseFloat(currentCategory.lng)];
                        marker["icon"] = GREEN_CIRCLE_ICON;
                        if(keyName == "river"){
                            marker["name"] = currentCategory.name;
                        } else if(keyName == "coast"){
                            marker["name"] = currentCategory.beach.name
                        }
                        this.setState({selectedCategory:keyName,currentCategory:currentCategory,currentMarkers:[marker]});
                        this.fitAllMarkers(70);
                    } else {
                        this.setState({selectedCategory:keyName,currentCategory:currentCategory,currentMarkers:[]});
                    }
                    break;
                case "glaciers":
                case "volcanoes":
                case "peaks":
                case "mountains":
                    var allMarkers = [];
                    for(var oneIdx in currentCategory){
                        let currentItem = currentCategory[oneIdx];
                        var oneMarker = {};
                        oneMarker["pos"] = [parseFloat(currentItem.lat), parseFloat(currentItem.lng)];
                        if(keyName == "peaks"){
                            oneMarker["icon"] = PEAK_ICON;
                        } else if (keyName == "mountains"){
                            oneMarker["icon"] = MOUNTAIN_ICON;
                        } else if (keyName == "glaciers"){
                            oneMarker["icon"] = GLACIER_ICON;
                        } else if (keyName == "volcanoes"){
                            oneMarker["icon"] = VOLCANO_ICON;
                        } else {
                            oneMarker["icon"] = GREEN_CIRCLE_ICON;
                        }
                        oneMarker["name"] = currentItem.name;
                        allMarkers.push(oneMarker);
                    }
                    this.setState({selectedCategory:keyName,currentCategory:currentCategory,currentMarkers:allMarkers});
                    this.fitAllMarkers(20);
                    break;
                case "vegetation":
                    this.refs.map.leafletElement.panTo(this.hotelCoordinates);
                    this.setState({selectedCategory:keyName,currentCategory:currentCategory,currentMarkers:[]});
                    break;
            }
        }
    }

    fitAllMarkers(paddingSize){
        setTimeout(()=>{
            if(this.refs.marker_0){
                var markerArray = [];
                for(var i=0;i<this.state.currentMarkers.length;i++){
                    markerArray.push(this.refs["marker_"+i].leafletElement);
                }
                markerArray.push(this.refs.main.leafletElement);
                var group = featureGroup(markerArray);
                this.refs.map.leafletElement.fitBounds(group.getBounds(),{padding:[paddingSize,paddingSize]});
                this.currentBounds = group.getBounds();
                this.currentPadding = paddingSize;
                setTimeout(()=>{
                    //code to show the first popup automatically
                    if(this.refs.popup_0){
                        this.refs.popup_0.leafletElement.setLatLng(this.refs["marker_0"].leafletElement.getLatLng());
                        this.refs.map.leafletElement.openPopup(this.refs.popup_0.leafletElement);
                    }
                },300);
            }
        },300); 
    }

    getHelpingText(){
        if(this.state.selectedCategory == "coast"){
            if(this.state.currentCategory.distance !== ""){
                let beach = "";
                if(this.state.currentCategory.beach.exists == true){
                    if(this.state.currentCategory.beach.name !== ""){
                        beach = "Beach available (" + this.state.currentCategory.beach.name + ")";
                    } else {
                        beach = beach = "Beach available";
                    }
                }
                return(
                    <div className="map-info">
                        <div className="circle-marker"/>
                        <div className="helping-text">
                            Nearest Coastline (~ {parseFloat(this.state.currentCategory.distance/1000).toFixed(2)} kms)
                            <br/>
                            <span className="beach-info">{beach}</span>
                        </div>
                    </div>
                );
            }
        } else if(this.state.selectedCategory == "river"){
            if(this.state.currentCategory.distance !== ""){
                return(
                    <div className="map-info">
                        <div className="circle-marker"/>
                        <div className="helping-text">
                            Nearest River (~ {parseFloat(this.state.currentCategory.distance/1000).toFixed(2)} kms)
                        </div>
                    </div>
                );
            }
        } else if(this.state.selectedCategory == "peaks"){
            return(
                <div className="map-info">
                    <div className="peak-marker" style={{"width":"30px","height":"30px"}}/>
                    <div className="helping-text">
                        {this.state.currentCategory.length} nearby
                        {this.state.currentCategory.length == 1 ? " peak" : " peaks"}
                    </div>
                </div>
            );
        } else if(this.state.selectedCategory == "glaciers"){
            return(
                <div className="map-info">
                    <div className="glacier-marker" style={{"width":"30px","height":"30px"}}/>
                    <div className="helping-text">
                        {this.state.currentCategory.length} nearby 
                        {this.state.currentCategory.length == 1 ? " glacier" : " glaciers"}
                    </div>
                </div>
            );
        } else if(this.state.selectedCategory == "volcanoes"){
            return(
                <div className="map-info">
                    <div className="volcano-marker" style={{"width":"30px","height":"30px"}}/>
                    <div className="helping-text">
                        {this.state.currentCategory.length} nearby Volcanic 
                        {this.state.currentCategory.length == 1 ? " peak" : " peaks"}
                    </div>
                </div>
            );
        } else if(this.state.selectedCategory == "mountains"){
            return(
                <div className="map-info">
                    <div className="mountain-marker" style={{"width":"30px","height":"30px"}}/>
                    <div className="helping-text">
                        {this.state.currentCategory.length} nearby 
                        {this.state.currentCategory.length == 1 ? " mountain" : " mountains"}
                    </div>
                </div>
            );
        } else if(this.state.selectedCategory == "vegetation"){
            return(
                <div className="map-info">
                    <div className="vegetation-marker"/>
                    <div className="helping-text">
                        <span style={{"textTransform":"capitalize"}}>{this.state.currentCategory.score + " "}</span>
                        <span>{this.state.currentCategory.text}</span> around this area
                    </div>
                </div>
            );
        }
    }

    centerMapOrFitBounds(){
        if(this.state.selectedCategory == "" || this.state.selectedCategory == "vegetation"){
            this.refs.map.leafletElement.panTo(this.hotelCoordinates);
        } else {
            this.refs.map.leafletElement.fitBounds(this.currentBounds,{padding:[this.currentPadding,this.currentPadding]});
        }
    }

    handleLanguageChange = (event,index,value) =>{
        this.setState({selectedDropdownLanguage:value, selectedLanguageIndex:-1})
    }

    render() {
        let wrapperStyles = {
            width:this.props.inputData.width,
            height:this.props.inputData.height,
            position:"fixed",
            zIndex:this.props.inputData.zindex,
            top:this.props.inputData.widget_position.top,
            left:this.props.inputData.widget_position.left,
            transform:"translate(-" + this.props.inputData.widget_position.left + ", -" + this.props.inputData.widget_position.top+")"
        }
        return (
            <div className="location__widget__nearby__wrapper" style={wrapperStyles}>
                {
                    this.state.loading ?
                    <LoaderComponent/>
                    :
                    <div className="location__widget__mask">
                        <div className="location__text__section">
                            <div className="language-section">
                                {
                                    LANGUAGES_1.map((language,index)=>{
                                        return(
                                            <div 
                                                className={"lang-btn" + (this.state.selectedLanguageIndex == index ? " selected" : "")} 
                                                key={index}
                                                onClick={(event)=>{
                                                    this.setState({selectedLanguageIndex : index,selectedDropdownLanguage:""})
                                                }}>
                                                {language.name}
                                            </div>
                                        );  
                                    })
                                }
                                <SelectField
                                    hintText="Select a language"
                                    fullWidth={true}
                                    value={this.state.selectedDropdownLanguage}
                                    onChange={this.handleLanguageChange}>
                                        {
                                            LANGUAGES_2.map((language,index)=>{
                                                return(
                                                    <MenuItem value={language.name} primaryText={language.name} key={index} />
                                                )
                                            })
                                        }
                                    </SelectField>
                            </div>
                            <div className="location__greeting">
                                Hotel East Bourne, Shimla
                            </div>
                            <div className="location__question" style={{"display":"none"}}>
                                Click on the following to know more
                            </div>
                            <div className="location__ans__wrap">
                                {
                                    Object.keys(this.state.nearbyData).map((keyName)=>{
                                        let currentObj = this.state.nearbyData[keyName];
                                        return(
                                            <div className={`__answer__item` + (this.state.selectedCategory == keyName ? " selected" : "")} key={keyName} onClick={(event)=>{
                                                event.stopPropagation();
                                                event.preventDefault();
                                                this.handleCategoryClick(keyName, currentObj);
                                            }}>
                                                {keyName.toUpperCase()} 
                                            </div> 
                                        );
                                    })
                                }
                            </div>
                            {
                                this.state.currentCategory != null ?
                                    this.getHelpingText()
                                :
                                null
                            }
                        </div>
                        <div className="location__map__section">
                            <Map center={[this.hotelCoordinates[0],this.hotelCoordinates[1]]} zoom={12} style={{"width":"100%","height":"100%"}} scrollWheelZoom={false} ref="map">
                                <TileLayer
                                url="https://api.mapbox.com/styles/v1/vsvanshi/cjbgp9nfw7gra2rmo76sd8gd0/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoidnN2YW5zaGkiLCJhIjoiY2l0YnF3Yzl1MDd0YjJvczZiempmczc0NyJ9.PUqEP-Mrl2Fxl82v6jyCOA"/>
                                    <Marker position={[this.hotelCoordinates[0],this.hotelCoordinates[1]]} icon={HOTEL_ICON} ref="main"/>
                                    {
                                        this.state.currentMarkers.map((_marker,index)=>{
                                            return(
                                                <Marker key={index} position={_marker.pos} icon={_marker.icon} ref={"marker_"+index}>
                                                    {
                                                        _marker.name !== ""?
                                                        <Popup ref={"popup_" + index}>
                                                            <span>{_marker.name}</span>
                                                        </Popup>
                                                        :
                                                        null
                                                    }   
                                                </Marker>
                                            );
                                        })
                                    }
                            </Map>
                            <div className="recenter" onClick={(event)=>{
                                this.centerMapOrFitBounds();
                            }}/>
                            <div className="bottom-info">
                                <div className="bt-info">
                                    Total malls nearby : 20
                                </div>
                                <div className="bt-info">
                                    Total malls nearby : 20
                                </div>
                            </div>
                        </div>
                    </div>   
                }
            </div>
        );
    }
}
LocationWidgetNearby.propTypes = {
    inputData : PropTypes.object
};