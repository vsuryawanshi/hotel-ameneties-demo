import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';

export default class ScratchCardSettings extends Component {
    constructor(props){
        super(props);
        this.state = {
            scratchImageUrl : "",
            greeting:"",
            customerName:"",
            offerText:"",
            offerTime:"",
            buttonLabel:"",
            backgroundImage:""
        }
    }
    render() {
        return (
            <div className="setting-form">
                <div className="hor-row">
                    <div className="inp-wrap img">
                        <TextField id="ss" label="Scratch Image URL" type="text" margin="dense" value={this.state.scratchImageUrl} onChange={event => this.setState({scratchImageUrl:event.target.value})}/>
                        <RaisedButton label="Set" color="primary" onClick={event => this.props.setScratchImageUrl(this.state.scratchImageUrl)}/>
                    </div>
                    <div className="inp-wrap img">
                        <TextField id="ss1" label="Background Image URL" type="text" margin="dense" value={this.state.backgroundImage} onChange={event => this.setState({backgroundImage:event.target.value})}/>
                        <RaisedButton label="Set" color="primary" onClick={event => this.props.setBackgroundImageUrl(this.state.backgroundImage)}/>
                    </div>
                </div>
                <div className="hor-row">
                    <div className="inp-wrap">
                        <TextField id="ss2" label="Greeting" type="text" margin="dense" value={this.state.greeting} onChange={event => this.setState({greeting:event.target.value})}/>
                        <RaisedButton label="Set" color="primary" onClick={event => this.props.setGreeting(this.state.greeting)}/>
                    </div>
                    <div className="inp-wrap">
                        <TextField  id="ss3" label="Customer Name" type="text" margin="dense" value={this.state.customerName} onChange={event => this.setState({customerName:event.target.value})}/>
                        <RaisedButton label="Set" color="primary" onClick={event => this.props.setCustomerName(this.state.customerName)}/>
                    </div>
                    <div className="inp-wrap">
                        <TextField id="ss4" label="Offer Text" type="text" margin="dense" value={this.state.offerText} onChange={event => this.setState({offerText:event.target.value})}/>
                        <RaisedButton label="Set" color="primary" onClick={event => this.props.setOfferText(this.state.offerText)}/>
                    </div>
                    <div className="inp-wrap">
                        <TextField id="ss5" label="Offer Time" type="text" margin="dense" value={this.state.offerTime} onChange={event => this.setState({offerTime:event.target.value})}/>
                        <RaisedButton label="Set" color="primary" onClick={event => this.props.setOfferTime(this.state.offerTime)}/>
                        <div className="pretty p-icon p-round p-bigger" style={{"display":"none"}}>
                            <input type="checkbox" onChange={(event)=>{
                                this.props.flashOffer(event.target.checked);
                            }} />
                            <div className="state p-info">
                                <i className="icon material-icons"></i>
                                <label>Flash Offer</label>
                            </div>
                        </div>
                    </div>
                    <div className="inp-wrap">
                        <TextField id="ss6" label="Button Label" type="text" margin="dense" value={this.state.buttonLabel} onChange={event => this.setState({buttonLabel:event.target.value})}/>
                        <RaisedButton label="Set" primary={true} onClick={event => this.props.setButtonLabel(this.state.buttonLabel)}/>
                    </div>
                </div>
            </div> 
        );
    }
}