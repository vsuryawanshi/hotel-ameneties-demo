import React, {Component} from "react";
import ReactDOM from "react-dom";


const SCROLL_DURATION = 300;
const SCROLL_THRESHOLD = 20;

export default class VerticalCarousal extends Component{
    constructor(props){
        super(props);
        this.currentPage = 0;
        this.vhHeight = window.innerHeight;
        this.numberOfChildren = React.Children.count(this.props.children);
        this.holderHeight = this.numberOfChildren * this.vhHeight;
        this.scrolling = false;
        this.state = {
            currentDot : 0,
            temp:true
        };
    }

    scrollToPos(element, to, duration) {
        var start = element.scrollTop,
            change = to - start,
            currentTime = 0,
            increment = 20;
    
        var animateScroll = function(){        
            currentTime += increment;
            var val = Math.easeInOutQuad(currentTime, start, change, duration);
            element.scrollTop = val;
            if(currentTime < duration) {
                setTimeout(animateScroll, increment);
            }
        };
        animateScroll();
    }

    goToPosition(position){
        var context = this;
        if(position >= 0 && position < this.numberOfChildren && this.currentPage != position && !this.scrolling){
            context.props.sectionLoadStarted(position);
            this.scrolling = true;
            this.currentPage = position;
            this.scrollToPos(ReactDOM.findDOMNode(this.refs.scroller),(this.currentPage)*this.vhHeight,SCROLL_DURATION);
            this.setState({
                currentDot:position
            });
            setTimeout(()=>{
                context.scrolling = false;
                context.props.sectionLoaded(position);
            },800)
        }
    }

    handleMouse(mouseScollEvent){
        this.disableMouse();
        if(Math.abs(mouseScollEvent.deltaY) > SCROLL_THRESHOLD){
            if(mouseScollEvent.deltaY > 0){
                //scrolling downwards
                this.goToPosition(this.currentPage+1);
            } else {
                //upwards
                this.goToPosition(this.currentPage-1);
            }
        }
    }

    enableMouse(){
        window.addEventListener("mousewheel", this.handleMouse.bind(this));
        window.addEventListener("DOMMouseScroll", this.handleMouse.bind(this));
        window.addEventListener("wheel", this.handleMouse.bind(this));
    }

    disableMouse(){
        window.removeEventListener("mousewheel", this.handleMouse.bind(this));
        window.removeEventListener("DOMMouseScroll",this.handleMouse.bind(this));
        window.removeEventListener("wheel",this.handleMouse.bind(this));
    }

    enableKeyBoard(){
        window.addEventListener("keydown", (event)=>{
            if(event.keyCode == "38"){
                //up arrow
                this.goToPosition(this.currentPage-1);
            } else if (event.keyCode == "40"){
                //down arrow
                this.goToPosition(this.currentPage+1);
            }
        })
    }

    componentDidMount() {
        this.enableMouse();
        this.enableKeyBoard();
        window.addEventListener("resize",()=>{
            this.setState({temp:!this.state.temp})
        })
    }
    componentWillUnmount(){
        window.removeEventListener("resize",()=>{});
    }

    render(){
        this.vhHeight = window.innerHeight;
        this.numberOfChildren = React.Children.count(this.props.children);
        this.holderHeight = this.numberOfChildren * this.vhHeight;
        return(
            <div className="vc_parent" style={{"height":this.vhHeight+"px"}} ref="scroller">
                <div className="vc_scroller" style={{"height":this.holderHeight+"px"}}>
                    {
                        React.Children.map(this.props.children, (_child,i) =>{
                            return(
                                <div className="vc_content_holder" style={{"height":this.vhHeight+"px"}}>
                                    {_child}
                                </div>
                            );
                        })
                    }
                </div>
                <div className="vc_sc_buttons">
                    {
                        React.Children.map(this.props.children, (_child,i) =>{
                            return(
                                <div className={`vc_dot` + (this.state.currentDot == i ? " filled" : "")} onClick={(event)=>{
                                    event.preventDefault();
                                    event.stopPropagation();
                                    this.goToPosition(i);
                                }}/>
                            );
                        })
                    }
                </div>
            </div>
        )
    }
}

Math.easeInOutQuad = function (t, b, c, d) {
    t /= d/2;
    if (t < 1) return c/2*t*t + b;
    t--;
    return -c/2 * (t*(t-2) - 1) + b;
};