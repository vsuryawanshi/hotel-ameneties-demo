import React, { Component } from 'react';
import MapComponent from "./Common/MapComponent";
import CarouselComponent from "./Common/CarousalComponent";
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import LStrings from "../common/string_resources";

const LANGUAGES_1 = [
    {
        "name":"English",
        "code":"en"
    },
    {
        "name":"हिंदी",
        "code":"hi"
    }
];
const LANGUAGES_2 = [
    {
        "name":"Punjabi"
    },
    {
        "name":"Kannada"
    },
    {
        "name":"Telugu"
    }
];

export default class Chatbot extends Component {
    state = {
        selectedLanguageIndex:0,
        selectedDropdownLanguageIndex:0,
        selectedDropdownLanguage:""
    }
    render() {
        let wrapperStyles = {
            width:this.props.inputData.width,
            height:this.props.inputData.height,
            position:"fixed",
            zIndex:this.props.inputData.zindex,
            top:this.props.inputData.widget_position.top,
            left:this.props.inputData.widget_position.left,
            transform:"translate(-" + this.props.inputData.widget_position.left + ", -" + this.props.inputData.widget_position.top+")"
        }
        return (
            <div className="chatbot__wrapper" style={wrapperStyles}>
                <div className="chatbot__header">
                    <div className="chatbot__header__content">
                        <div className="header__content">
                            {LStrings.hotelName}
                            <div className="sub">{LStrings.welcomeText}</div> 
                        </div>
                    </div>
                    <div className="chatbot__header_close"/>
                </div>
                <div className="chatbot__content">
                    <div className="scroll-wrap">
                        <div className="chat__item text">
                            {LStrings.msg1}
                        </div>   
                        <div className="chat__item map" >
                            <MapComponent/>
                        </div>  
                        <div className="chat__item text" >
                            <div className="row_with_icon">
                                <img src={require("../images/mountain.svg")} className="row-img"/>
                                <div className="row-text">{LStrings.msg2}</div>
                            </div>
                            <div className="row_with_icon">
                                <img src={require("../images/shopping-mall.svg")} className="row-img"/>
                                <div className="row-text">{LStrings.msg3}</div>
                            </div>
                            <div className="row_with_icon">
                                <img src={require("../images/shopping-mall.svg")} className="row-img"/>
                                <div className="row-text">{LStrings.msg4}</div>
                            </div>
                            <div className="row_with_icon">
                                <img src={require("../images/food.svg")} className="row-img"/>
                                <div className="row-text">{LStrings.msg5}</div>
                            </div>
                        </div> 
                        <div className="chat__item text" style={{"display":"none"}}>
                            {LStrings.amenitiesMsg}
                        </div>   
                        <div className="chat__item gallery" style={{"display":"none"}}>
                            <CarouselComponent />
                        </div> 
                    </div>
                </div>
                <div className="chatbot__footer">
                    <div className="language-section">
                        <div className="flbl">{LStrings.langSelect} :</div>
                        {
                            LANGUAGES_1.map((language,index)=>{
                                return(
                                    <div 
                                        className={"lang-btn" + (this.state.selectedLanguageIndex == index ? " selected" : "")} 
                                        key={index}
                                        onClick={(event)=>{
                                            LStrings.setLanguage(language.code)
                                            this.setState({selectedLanguageIndex : index,selectedDropdownLanguage:""})
                                        }}>
                                        {language.name}
                                    </div>
                                );  
                            })
                        }
                    </div>    
                </div>
            </div>
        );
    }
}