import React, { Component } from 'react';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';

const MAP = {
    0:"Hotel Mode",
    1:"Location Mode",
    2:"Backpacker Mode",
    3:"Mix Mode"
}
  
export default class ModeSelector extends Component {
    state = {
        selectedOption:2
    }
    render() {
        return (
            <div className="ms-wrap">
                <div className="lbl">{this.props.label || "Mode :"}</div>

                    {
                        Object.keys(MAP).map((key)=>{
                            return(
                                <div className="woo-radio-wrap" key={key}>
                                    <label>
                                        <input 
                                            type="radio" 
                                            name="mode" 
                                            className="rad"
                                            value={key} 
                                            checked={this.state.selectedOption == key}
                                            onChange={(event)=>{
                                                this.setState({selectedOption:event.target.value});
                                            }}/>{MAP[key]}
                                    </label>
                                </div>
                            );
                        })
                    }
            </div>
        );
    }
}