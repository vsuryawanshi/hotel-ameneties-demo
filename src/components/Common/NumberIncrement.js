import React, { Component } from 'react';

export default class NumberIncrement extends Component {
    constructor(props){
        super(props);
        this.state = {
            currentValue:this.props.startValue || 0
        }
    }
    render() {
        let label = this.props.label || "Label";
        return (
            <div className="nu-wrap">
                {
                    this.props.icon ?
                    <img src={this.props.icon} className="nu-img"/>
                    :
                    null
                }
                <div className="nu-label">{label}</div>
                <div className="nu-calc">
                    <div className="nu-ico minus" onClick={(event)=>{
                        event.preventDefault();
                        event.stopPropagation();
                        if(this.state.currentValue > 0){
                            var newVal = this.state.currentValue -1;
                            this.setState({
                                currentValue:newVal
                            },()=>{
                                this.props.onValueChanged(this.state.currentValue);
                            });
                        }
                    }}/>
                    <input type="text" className="nu-num" value={this.state.currentValue}/>
                    <div className="nu-ico plus" onClick={(event)=>{
                        event.preventDefault();
                        event.stopPropagation();
                        var newVal = this.state.currentValue + 1;
                        this.setState({
                            currentValue:newVal
                        },()=>{
                            this.props.onValueChanged(this.state.currentValue); 
                        });
                    }}/>
                </div>
            </div>
        );
    }
}