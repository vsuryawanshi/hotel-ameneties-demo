import React from "react";

const Loader = () => {
    return(
        <div className="__widget__loader">
            <div className="__loader"/>
        </div>
    );
}
export default Loader;