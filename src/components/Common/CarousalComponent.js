import React, {Component} from "react";
import Slider from "react-slick";

const GALLERY = [
    {
        text:"We offer candle light dinner for the couples",
        image:"https://flyinheaven.com/images/crazy%20travellers/candle%20light/manali/3.jpg"
    },
    {
        text:"Uninterrupted views of the Indian Ocean",
        image:"http://www.gili-lankanfushi.com/wp-content/uploads/2017/10/2.2_the-private-reserve_02.jpg"
    }
]
export default class CarouselComponent extends Component{
    constructor(props){
        super(props);
    }

    render(){
        const settings = {
            dots:false,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1
        };
        return(
            <div className="carousel-container">
                <Slider {...settings}>
                    {
                        GALLERY.map((item,index)=>{
                            return(
                                <div className="carousel-item" key={index}>
                                    <div className="carousal-img" style={{"backgroundImage":"url(" + item.image +")"}}/>
                                    <div className="carousal-txt">{item.text}</div>
                                </div>
                            );
                        })
                    }
                </Slider>   
            </div>
        );
    }
}