import React, { Component } from 'react';
import { divIcon,featureGroup,popup} from 'leaflet';
import { Map, Marker, Popup, TileLayer } from 'react-leaflet';

const HOTEL_ICON = divIcon({className: 'location-marker',html:"<div class='hotel-marker'></div>",iconAnchor:[30,30]});
const MOUNTAIN_ICON = divIcon({className: 'location-marker',html:"<div class='mountain-marker'></div>",iconAnchor:[7,7]});
const MALL_ICON = divIcon({className: 'location-marker',html:"<div class='mall-marker'></div>",iconAnchor:[7,7]});
const RESTAURANT_ICON = divIcon({className: 'location-marker',html:"<div class='food-marker'></div>",iconAnchor:[7,7]});

const MALLS = [
    {
        lat:31.101028, 
        lng :77.152595
    },
    {
        lat:31.074980,
        lng :77.141759
    }
];

const MOUNTAINS = [
    {
        lat :31.111993, 
        lng :77.212765
    }
];

const RESTAURANTS = [
    {
        lat : 31.073108, 
        lng : 77.200015
    }
]

export default class MapComponent extends Component {

    hotelCoordinates = [31.089233, 77.174650]

    centerMapOrFitBounds = () => {
        this.refs.map.leafletElement.panTo(this.hotelCoordinates);
    }

    render() {
        let mapWrapperStyles = {
            width:"100%",
            height:"100%",
            borderRadius:"5px",
            overflow:"hidden",
            position:"relative",
            border:"1px solid #ccc"
        };
        
        let centerButtonStyles = {
            position: "absolute",
            top:"10px",
            right: "10px",
            width: "30px",
            height: "30px",
            borderRadius: "50%",
            backgroundColor: "#fff",
            boxShadow: "0px 2px 20px 0px rgba(0, 0, 0, 0.4)",
            backgroundImage: "url(../images/center.svg)",
            backgroundSize: "20px 20px",
            backgroundPosition: "center",
            backgroundRepeat: "no-repeat",
            zIndex: "2000",
            cursor: "pointer"
        }
        return (
            <div className="map_component_wrapper" style={mapWrapperStyles}>
                <Map center={[this.hotelCoordinates[0],this.hotelCoordinates[1]]} zoom={12} style={{"width":"100%","height":"100%"}} scrollWheelZoom={false} ref="map">
                    <TileLayer
                    url="https://api.mapbox.com/styles/v1/vsvanshi/cjcfwu9cm3g912snuh5v1rpau/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoidnN2YW5zaGkiLCJhIjoiY2l0YnF3Yzl1MDd0YjJvczZiempmczc0NyJ9.PUqEP-Mrl2Fxl82v6jyCOA"/>
                        <Marker position={[this.hotelCoordinates[0],this.hotelCoordinates[1]]} icon={HOTEL_ICON} ref="main"/>
                        {
                            MALLS.map((mall,index)=>{
                                return(
                                    <Marker position={[mall.lat,mall.lng]} icon={MALL_ICON} ref="mall" key={"mall"+index}/>
                                );
                            })
                        }
                        {
                            MOUNTAINS.map((mall,index)=>{
                                return(
                                    <Marker position={[mall.lat,mall.lng]} icon={MOUNTAIN_ICON} ref="mall" key={"mountain"+index}/>
                                );
                            })
                        }
                        {
                            RESTAURANTS.map((mall,index)=>{
                                return(
                                    <Marker position={[mall.lat,mall.lng]} icon={RESTAURANT_ICON} ref="mall" key={"mountain"+index}/>
                                );
                            })
                        }
                        
                </Map>
                <div className="map__center" onClick={this.centerMapOrFitBounds} style={centerButtonStyles}/>
            </div>
        );
    }
}