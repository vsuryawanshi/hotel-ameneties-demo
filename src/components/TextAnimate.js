import React, {Component} from "react";
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import ReactDOM from 'react-dom';

export default class TextAnimate extends Component {
    constructor(props){
        super(props);
        this.state = {
            selectedText : this.props.items[0],
            items:new Array(this.props.items[0])
        }
        this.itemCounter = 0;
    }

    handleAdd() {
        let newItems = this.state.items;
        newItems.pop();
        newItems.push(this.props.items[this.itemCounter++]);
        this.setState({items: newItems});
        if(this.itemCounter == this.props.items.length){
            this.itemCounter = 0;
        }
        var elem = ReactDOM.findDOMNode(this.itemRef);
        var wrapElement = ReactDOM.findDOMNode(this.refs.animwrap);
        wrapElement.style.height = elem.clientHeight + "px";

    }

    startAnimation(){
        var context = this;
        if(this.props.items.length > 1){
            this.handleAdd();
            this.intervalTimer = setInterval(()=>{
               context.handleAdd(); 
            },this.props.timeout);
        }
    }

    componentDidMount() {
        this.startAnimation();
    }

    stopComponent(){
        this.setState({selectedText:null,items:[]})
        clearInterval(this.intervalTimer);
    }

    startComponent(){
        this.setState({selectedText:this.props.items[0],items:new Array(this.props.items[0])})
        this.startAnimation();
    }

    render(){
        const items = this.state.items.map((item, i) => (
            <div key={item} className="animation-item" ref={(elem) => {this.itemRef = elem}}>
                {item}
            </div>
        ));      
        return(    
            <ReactCSSTransitionGroup
                component="div"
                ref="animwrap"
                className="animated-list"
                transitionName="fadeIn"
                transitionAppear={true}
                transitionAppearTimeout={1500}
                transitionEnterTimeout={1500}
                transitionLeaveTimeout={1500}
                transitionLeave={true}>
                {items}
            </ReactCSSTransitionGroup>
        );
    }
}