/* eslint-disable import/default */

import React from 'react';
import { render } from 'react-dom';
import { Router, hashHistory } from 'react-router';
import routes from './routes';
require('./favicon.ico');
import './styles/main.scss';

delete Router.prototype.unstable_handleError;
render(
    <Router routes={routes} history={hashHistory} />, document.getElementById('app')
);
