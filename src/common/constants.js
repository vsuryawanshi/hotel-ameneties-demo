export const HOME_LOCATION = [12.908132, 77.678555];
export const MENU_ITEMS = [
    {
        "name":"Product",
        "type":"internal",
        "index":1
    },
    {
        "name":"Technology",
        "type":"internal",
        "index":2
    },
    {
        "name":"About us",
        "type":"internal",
        "index":3
    },
    {
        "name":"Contact",
        "type":"internal",
        "index":4
    },
    {
        "name":"Demo",
        "type":"button",
        "index":-1
    }
];

export const FIRST_ANIM = [
    {
        "text":"Smiling woman",
        "image":require("../images/happy.svg"),
        "percent":"87%"
    },{
        "text":"Smiling man",
        "image":require("../images/happy.svg"),
        "percent":"80%"
    },{
        "text":"Burning candle",
        "image":require("../images/candle.svg"),
        "percent":"82%"
    },{
        "text":"Lamp",
        "image":require("../images/lamp.svg"),
        "percent":"75%"
    },{
        "text":"Woman age : 26",
        "image":require("../images/age.svg"),
        "percent":"65%"
    },{
        "text":"Man age : 30",
        "image":require("../images/age.svg"),
        "percent":"60%"
    },{
        "text":"Romantic scene",
        "image":require("../images/snuggle.svg"),
        "percent":"90%"
    }
];

export const SECOND_ANIM = [
    {
        "text":"Drinking Illegal",
        "image":require("../images/pint.svg"),
        "percent":"84%"
    },{
        "text":"Horrific Location",
        "image":require("../images/marker.svg"),
        "percent":"89%"
    },{
        "text":"No Resort",
        "image":require("../images/resort.svg"),
        "percent":"76%"
    },{
        "text":"Old furniture",
        "image":require("../images/bunk-bed.svg"),
        "percent":"76%"
    },{
        "text":"Overall negative sentiments",
        "image":require("../images/sad.svg"),
        "percent":"95%"
    }   
]

export const THIRD_ANIM = [
    {
        "text":"Elevator",
        "image":require("../images/elevator.svg"),
        "percent":""
    },{
        "text":"In-room service",
        "image":require("../images/room-service.svg"),
        "percent":""
    },{
        "text":"Dietary restrictions",
        "image":require("../images/food.svg"),
        "percent":""
    },{
        "text":"In premise medical amenities",
        "image":require("../images/medical.svg"),
        "percent":""
    }   
]

export const FOURTH_ANIM = [
    {
        "text":"Crowded city",
        "image":require("../images/crowd.svg"),
        "percent":""
    },{
        "text":"Subway Available",
        "image":require("../images/metro.svg"),
        "percent":""
    },{
        "text":"Multicuisine food",
        "image":require("../images/groceries.svg"),
        "percent":""
    },{
        "text":"Languages",
        "image":require("../images/world.svg"),
        "percent":"English, Hindi"
    },{
        "text":"No beach",
        "image":require("../images/surf.svg"),
        "percent":""
    },{
        "text":"No mountains",
        "image":require("../images/mountain.svg"),
        "percent":""
    },{
        "text":"No snowfall",
        "image":require("../images/snowflakes.svg"),
        "percent":""
    }
]