import LocalizedStrings from 'react-localization';

let LStrings = new LocalizedStrings({
    en:{
      hotelName:"Hotel Oakwood Silver",
      welcomeText:"Welcomes you",
      msg1:"We are the best located Hotel in Shimla",
      msg2:"Mountain View",
      msg3:"2 malls nearby",
      msg4 : "Walking distance to Mall Road",
      msg5:"3 multicuisine restaurants including 1 italian",
      langSelect:"Select your Language",
      amenitiesMsg:"Kids love our hotel, especially our Kids Playing Zone"
    },
    hi: {
        hotelName:"होटल ईस्ट वेस्ट प्लाजा",
        welcomeText:"आपका स्वागत करता है",
        msg1:"होटल ईस्ट वेस्ट प्लाजा शिमला में सर्वश्रेष्ठ स्थित होटल हैं",
        msg2:"पर्वतों का दृष्य",
        msg3:"2 मॉल्स पास में ",
        msg4 : "मॉल रोड से चलने की दूरी",
        msg5:"1 इटालियन सहित 3 मल्टी क्वीज़ीन रेस्तरां",
        langSelect:"अपनी भाषा का चयन करें",
        amenitiesMsg:"बच्चे हमारे होटल को प्यार करते हैं, खासकर हमारे बच्चों को खेलना क्षेत्र"
    }
});

export default LStrings;