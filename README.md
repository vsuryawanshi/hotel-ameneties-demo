# Mina Dashboard


This is inference engine.

Tech Stack
==========

This web app has been made in [ReactJS](https://facebook.github.io/react/).


Installation / Running
----------------------

1. Clone this repository `git clone https://vsuryawanshi@bitbucket.org/hotels_ai/mina-dasboard.git`
2. Change into the new directory `cd <your cloned repository>`
3. Then `npm install` to install the dependencies.
4. `npm start` will run the app in development mode, watching all the files.
5. Open the browser and see the development version of app running at [http://localhost:7000](http://localhost:7000)
6. To make a production build `npm run build`, then a production version will be run in the browser automatically.
7. To deploy the production version, simply copy the `dist/` directory to any server you want. 


License
-------
Woocation - Open Source
